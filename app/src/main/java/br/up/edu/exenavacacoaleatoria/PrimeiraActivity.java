package br.up.edu.exenavacacoaleatoria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class PrimeiraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primeira);

        Intent intent = getIntent();
        String origem = intent.getStringExtra("id");
        TextView caixa = (TextView) findViewById(R.id.txt1);
        caixa.setText(origem);
    }

    public void vai(View v) {
        int numero;
        do {
            Random r = new Random();
            numero = r.nextInt(5);
            numero++;
        } while (numero == 1);

        Intent intent = null;
        switch (numero) {
            case 2:
                intent = new Intent(this, SegundaAcitivity.class);
                break;
            case 3:
                intent = new Intent(this, TerceiraActivity.class);
                break;
            case 4:
                intent = new Intent(this, QuartaActivity.class);
                break;
            case 5:
                intent = new Intent(this, QuintaActivity.class);
                break;
        }
        intent.putExtra("id", "1");
        startActivity(intent);
    }

    public void volta(View v){
        finish();
    }
}